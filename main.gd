extends Node3D

const RES_DEFAULT = 1.0
const RES_QUEST_2 = 1.2766
const RES_PICO_4 = 1.4365
const RES_QUEST_3 = 1.468

func _ready():
	get_viewport().use_xr = true
	var xr_interface : OpenXRInterface = XRServer.primary_interface
	var refresh_rates = xr_interface.get_available_display_refresh_rates()
	xr_interface.set_display_refresh_rate(refresh_rates.pop_back())
	xr_interface.render_target_size_multiplier = RES_DEFAULT
